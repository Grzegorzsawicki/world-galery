package com.example.worldgallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    Context context;
    ArrayList<Photo> photoArrayList;
    GalleryAdapterListener galleryAdapterListener;

    public interface GalleryAdapterListener{
        void onGalleryClick(int position);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        TextView title;
        ImageView imageView;
        GalleryAdapterListener galleryAdapterListener;
        public ViewHolder(@NonNull View itemView,GalleryAdapterListener galleryAdapterListener){
            super(itemView);
           // title=itemView.findViewById(R.id.title1);
            imageView=itemView.findViewById(R.id.photo);
            this.galleryAdapterListener=galleryAdapterListener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            galleryAdapterListener.onGalleryClick(getAdapterPosition());
        }
    }
    public GalleryAdapter(Context context,ArrayList<Photo> photoArrayList,GalleryAdapterListener galleryAdapterListener){
        this.context= context;
        this.photoArrayList= photoArrayList;
        this.galleryAdapterListener = galleryAdapterListener;
    }
    @NonNull
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.single_photo,parent,false);
        ViewHolder viewHolder = new ViewHolder(view,galleryAdapterListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder holder, int position) {
        //holder.title.setText("Title nr "+Integer.toString(position));
        //holder.title.setText(photoArrayList.get(position).getInfo());
        holder.imageView.setImageResource(R.drawable.add_button);

        byte[] image = photoArrayList.get(position).getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(image,0,image.length);
        holder.imageView.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return photoArrayList.size();
    }
}
