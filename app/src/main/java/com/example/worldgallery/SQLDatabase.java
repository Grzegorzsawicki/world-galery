package com.example.worldgallery;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;

public class SQLDatabase extends SQLiteOpenHelper {

    public static SQLDatabase sqlDatabase;

    public SQLDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        queryDatabase("CREATE TABLE IF NOT EXISTS IMAGE (Id INTEGER PRIMARY KEY AUTOINCREMENT, Info VARCHAR, Image BLOG, Latitude DOUBLE, Longnitude DOUBLE)");
    }

    public void queryDatabase(String query){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(query);
    }

    public void insertToDatabase(String info, Drawable drawable, double latitude, double longitude){
        SQLiteDatabase database = getWritableDatabase();
        String query = "INSERT INTO IMAGE VALUES (NULL,?,?,?,?)";
        SQLiteStatement statement = database.compileStatement(query);
        statement.clearBindings();
        statement.bindString(1, info);
        statement.bindBlob(2,drawableToByte(drawable));
        statement.bindDouble(3,latitude);
        statement.bindDouble(4,longitude);

        statement.executeInsert();
    }

    public Cursor getDataFromDatabase(String query){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(query,null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public Photo getPhoto(int index){
        Cursor cursor= SQLDatabase.sqlDatabase.getDataFromDatabase("SELECT * FROM IMAGE");
        while(cursor.moveToNext()){
            int id = cursor.getInt(0);
            if(id==index){
                return new Photo(id,cursor.getString(1),cursor.getBlob(2),cursor.getLong(3),cursor.getLong(4));
            }
        }
        return null;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private byte[] drawableToByte(Drawable drawable){
        Bitmap bitmap= ((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
