package com.example.worldgallery;

import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainGalleryFragment extends Fragment implements GalleryAdapter.GalleryAdapterListener {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    FloatingActionButton floatingActionButton ;
    ArrayList<Photo> arrayList;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        MainActivity.fab.show();
        return inflater.inflate(R.layout.main_gallery, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SQLDatabase.sqlDatabase = new SQLDatabase(getContext(),"ImageDB.sqlite",null,1);


        ImageView imageView;
        //imageView.setImageResource(R.drawable.add_button);
        arrayList = new ArrayList<Photo>();

        //sqlDatabase.insertToDatabase("drugi test1",imageView.getDrawable());
        //sqlDatabase.insertToDatabase("trzeci test1",imageView.getDrawable());

        Cursor cursor= SQLDatabase.sqlDatabase.getDataFromDatabase("SELECT * FROM IMAGE");
        while(cursor.moveToNext()){
            int id = cursor.getInt(0);
            String info = cursor.getString(1);
            byte [] image = cursor.getBlob(2);
            Long latitude = cursor.getLong(3);
            Long longnitude = cursor.getLong(3);

            arrayList.add(new Photo(id,info,image,latitude, longnitude));
            Log.e(null,info);
        }

        recyclerView = view.findViewById(R.id.RecyclerView1);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        } else {
            layoutManager = new StaggeredGridLayoutManager(4,StaggeredGridLayoutManager.VERTICAL);
        }

        recyclerView.setLayoutManager(layoutManager);
        GalleryAdapter galleryAdapter = new GalleryAdapter(getContext(),arrayList,this);
        recyclerView.setAdapter(galleryAdapter);


        MainActivity.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(MainGalleryFragment.this)
                        .navigate(R.id.action_MainGalleryFragment_to_AddphotoFragment);
            }
        });





    }

    @Override
    public void onGalleryClick(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt("key", position+1);

        NavHostFragment.findNavController(MainGalleryFragment.this)
                .navigate(R.id.action_MainGalleryFragment_to_PhotoDetails,bundle);
    }
}