package com.example.worldgallery;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;

public class PhotoDetailsFragment extends Fragment implements OnMapReadyCallback{

    GoogleMap googleMap;
    ImageView imageView;
    TextView details;
    Photo photo;
    int index;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            index = bundle.getInt("key", 0);
        }
        return inflater.inflate(R.layout.photo_details_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        details = view.findViewById(R.id.textViewPhotoDetails);
        imageView= view.findViewById(R.id.imageViewPhotoDetails);
        photo=SQLDatabase.sqlDatabase.getPhoto(index);

        details = view.findViewById(R.id.textViewPhotoDetails);
        imageView= view.findViewById(R.id.imageViewPhotoDetails);

        Bitmap bitmap = BitmapFactory.decodeByteArray(photo.getImage(),0,photo.getImage().length);
        imageView.setImageBitmap(bitmap);
        details.setText("Description: "+photo.getInfo());



        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        view.findViewById(R.id.button_go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(PhotoDetailsFragment.this)
                        .navigate(R.id.action_PhotoDetails_to_MainGalleryFragment);
            }
        });
        MainActivity.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(PhotoDetailsFragment.this)
                        .navigate(R.id.action_PhotoDetails_to_AddphotoFragment);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap=googleMap;

        LatLng marker=new LatLng(photo.getLatitude(), photo.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(marker).title(getString(R.string.location)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
    }
}