package com.example.worldgallery;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Debug;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.getSystemService;

public class AddphotoFragment extends Fragment implements LocationListener {

    private LocationManager locationManager;
    ImageView photo;
    EditText description;
    private double longtitute,latitude;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        MainActivity.fab.hide();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.add_photo_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        photo=view.findViewById(R.id.ImageView_takenPhoto);
        description= view.findViewById(R.id.editText_description);
        view.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(AddphotoFragment.this)
                        .navigate(R.id.action_AddphotoFragment_to_MainGalleryFragment);
            }
        });
        view.findViewById(R.id.button_take_a_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(getActivity(),new String[]{ Manifest.permission.CAMERA},100);
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,100);
            }
        });
        MainActivity.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(photo.getDrawable()==null)

                NavHostFragment.findNavController(AddphotoFragment.this)
                        .navigate(R.id.action_AddphotoFragment_to_MainGalleryFragment);
            }
        });
        view.findViewById(R.id.button_submit_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(photo.getDrawable()==null){
                   Snackbar.make(view, R.string.no_photo_warning, Snackbar.LENGTH_LONG)
                           .setAction("Action", null).show();
                   return;
               }
               if(description.getText().toString().length()<2){
                   Snackbar.make(view, R.string.no_description_warning, Snackbar.LENGTH_LONG)
                           .setAction("Action", null).show();
                   return;
               }

               SQLDatabase.sqlDatabase.insertToDatabase(description.getText().toString(),photo.getDrawable(), latitude, longtitute);
               Snackbar.make(view, R.string.photo_added, Snackbar.LENGTH_LONG)
                       .setAction("Action", null).show();

               photo.setImageResource(R.drawable.samplephoto);

                NavHostFragment.findNavController(AddphotoFragment.this)
                       .navigate(R.id.action_AddphotoFragment_to_MainGalleryFragment);
            }
        });
        Object object = getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager = (LocationManager) object;
        if (ActivityCompat.checkSelfPermission(this.getContext()
                , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location= locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

        if(location==null)
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 5000, 5, this);
        location= locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

    }
    @Override
    public void onLocationChanged(@NonNull Location location) {

        locationManager.removeUpdates(this);
        this.longtitute = location.getLongitude();
        this.latitude = location.getLatitude();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode==RESULT_OK)
            if(requestCode==100){
                Bitmap image = (Bitmap)data.getExtras().get("data");
                photo.setImageBitmap(image);
            }
    }


}