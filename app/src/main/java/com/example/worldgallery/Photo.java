package com.example.worldgallery;

public class Photo {

    private  byte[] image;
    private  String info;
    private int id;
    private double latitude,longitude;
    public Photo(int id, String info, byte[] image,double latitude,double longitude){
        this.id=id;
        this.info = info;
        this.image = image;
        this.latitude =latitude;
        this.longitude = longitude;
    }

    public String getInfo(){
        return info;
    }
    public byte[] getImage(){
        return image;
    }

    public int getId(){
        return id;
    }
    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }
}
